
# MediCription


This is an app and web portal for mutual communication among doctors and their respective patients. 

It enables user to give appointments, schedule the reminders for appointments, manage more than one doctors and their respective dov=cuments, prescriptions and other important files.

It also comes with the daily reminder for medicines and other medications, defined by the user.

On the other hand, doctors can also manage and maintain their schedules for patients and save records online. 

## Features
For both doctors and patients:

* Save documents, records, medicine presciptions
* View the documents on the fly from anywhere over the Internet
* Share and transfer prescriptions and medication information
* Maintain scheduler for important reminders
* Set reminders for medicines or doctor/patient appointments
* Easy communication portal among doctors and patients over the Internet


