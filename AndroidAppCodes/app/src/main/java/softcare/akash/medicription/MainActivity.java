package softcare.akash.medicription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Dialog;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private long mLastClickTime = 0;

    public static final String mBroadcastStringAction = "com.truiton.broadcast.string";
    private IntentFilter mIntentFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicriotion);
        Button doctor=(Button)findViewById(R.id.doctor);
        Button patient= (Button)findViewById(R.id.patient);

//        list = (ListView) findViewById(R.id.report_list);
//        myList = new ArrayList<DoctorProfile>();


        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastStringAction);

        Intent serviceIntent = new Intent(this, MyFirebaseMessagingService.class);
        startService(serviceIntent);


        ///////////DOCTOR CODES /////////////////

        doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog= new Dialog(MainActivity.this);
                dialog.setTitle("Login");
                dialog.setContentView(R.layout.dialoge);
                dialog.show();


                final EditText user_id=(EditText)dialog.findViewById(R.id.user_id);
                final EditText password=(EditText)dialog.findViewById(R.id.password);
                Button submit=(Button)dialog.findViewById(R.id.submit);
                Button cancel=(Button)dialog.findViewById(R.id.cancel);
                submit.setOnClickListener(new View.OnClickListener(){

                    public void onClick(View view)
                    {
                        String x=user_id.getText().toString();
                        String y=password.getText().toString();


                        if (x.length() < 1 ||y.length() < 5 ) {
                            if (x.length() < 1)
                                Toast.makeText(getApplicationContext(), "Please input your name", Toast.LENGTH_SHORT).show();
                            if (y.length() < 5)
                                Toast.makeText(getApplicationContext(), "Your password should contain at least 5 characters", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),x,Toast.LENGTH_SHORT).show();
                            //dialog.cancel();
                        }
                        else {

                            DoctorMain doc1 = new DoctorMain(x,y);  //name pass send

                            checkD(x, y, "Doctor");

                            //startActivity(new Intent(getApplicationContext(), mainUI.class));
                        }
                        //Intent i = new Acti();
                        //startActivity(i);

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view)
                    {
                        dialog.cancel();
                    }
                });

            }
        });

        //////////PATIENT CODES//////////////////

        patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog= new Dialog(MainActivity.this);
                dialog.setTitle("Login");
                dialog.setContentView(R.layout.dialoge);
                dialog.show();

                final EditText user_id=(EditText)dialog.findViewById(R.id.user_id);
                final EditText password=(EditText)dialog.findViewById(R.id.password);
                Button submit=(Button)dialog.findViewById(R.id.submit);
                Button cancel=(Button)dialog.findViewById(R.id.cancel);
                submit.setOnClickListener(new View.OnClickListener(){

                    public void onClick(View view)
                    {


                        String x=user_id.getText().toString();
                        String y=password.getText().toString();
                        //Toast.makeText(getApplicationContext(),x,Toast.LENGTH_SHORT).show();
                        //dialog.cancel();

                        if (x.length() < 1 ||y.length() < 5 ) {
                            if (x.length() < 1)
                                Toast.makeText(getApplicationContext(), "Please input your name", Toast.LENGTH_SHORT).show();
                            if (y.length() < 5)
                                Toast.makeText(getApplicationContext(), "Your password should contain at least 5 characters", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),x,Toast.LENGTH_SHORT).show();
                            //dialog.cancel();
                        }
                        else {

                            PatientMain pat1 = new PatientMain(x, y);  //name pass send
                            startActivity(new Intent(getApplicationContext(), patientUI.class));
                        }
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view)
                    {
                        dialog.cancel();
                    }
                });

            }
        });

    } //onCreate ends here

    @Override
    public void onBackPressed(){

        mLastClickTime = SystemClock.elapsedRealtime();

        Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();

        //for(int i=0;i<1000000;i++);
        //super.onBackPressed();
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000)
        {
            finish();
            Intent intent = new Intent (Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else
            return;
    }


    //FIREBASE MSG RCV CODES

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(mBroadcastStringAction))
            {
                //text.setText(intent.getStringExtra("Data"));
                Toast.makeText(getApplicationContext(), intent.getStringExtra("Data") ,Toast.LENGTH_SHORT).show();
            }
        }
    };

    ////// LOG IN CODES/////////////

    //boolean check (String email, String pass , String type)
    public void checkD(final String email, final String pass, final String type )
    {
        Toast.makeText(getApplicationContext(), "Checking!", Toast.LENGTH_SHORT).show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Medi/appquery.php",
            new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response) {

                    Log.d("Response", response);
                    try {
                        String  check1 = response;   // return - 3

                        //Toast.makeText(getApplicationContext(), check1.length() , Toast.LENGTH_SHORT).show();
                        //int a = Integer.parseInt(check1);
                        //Toast.makeText(getApplicationContext(), a , Toast.LENGTH_SHORT).show();
//                                Log.d(check1, check1.toString());
                        //if ( Integer.parseInt(check1) >0  )
                       if ( check1.contains("1") || check1.contains("2") || check1.contains("3") || check1.contains("4") || check1.contains("5") || check1.contains("6") || check1.contains("7") || check1.contains("8") || check1.contains("9")  )
                        {
                            Toast.makeText(getApplicationContext(),check1 , Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), mainUI.class));
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Not matched with database" , Toast.LENGTH_SHORT).show();
                        //if (response ==)
                        //JSONArray jArray = new JSONArray(response);
//                        //json.setText(response);
//                        for (int i=0;i<jArray.length();i++)
//                        {
//                            JSONObject job = jArray.getJSONObject(i);
//                            //   showToast(job.toString());
//                            String id = job.getString("ID");
//                            String award = job.getString("AWARD");
//                            String education = job.getString("EDUCATION");
//                            String hospital = job.getString("HOSPITAL");
//                            String publication = job.getString("PUBLICATION");
//                            DoctorProfile temp = new DoctorProfile( id, award ,education, hospital, publication);
//                            myList.add(temp);
//                            listAdapter.notifyDataSetChanged();
                        //}
                    } catch (Exception e) {
                        e.printStackTrace();
                        //showToast(e.toString());
                    }
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    //showToast(error.toString());
                    Log.d("Error.Response", error.toString());
                }
            }
    ) {
        @Override
        protected Map<String, String> getParams()
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put("query_name", "getDoctorId");
            params.put("EMAIL", email);
            params.put("PASS", pass);
            params.put("TYPE", type );

            return params;
        }
    };    //post request ends by listening to response from the given php link & then putting up params
        queue.add(postRequest);
    }






}

