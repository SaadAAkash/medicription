package softcare.akash.medicription;

/**
 * Created by Akash on 09-Nov-16.
 */
public class DoctorProfile  {
    public String userID, award ,education, hospital, publication;
    public DoctorProfile(String ID, String award, String education, String hospital, String publication)
    {
        this.userID = ID;
        this.award=award;
        this.education=education;
        this.hospital = hospital;
        this.publication = publication;
    }
}

