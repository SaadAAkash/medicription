package softcare.akash.medicription;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;


/**
 * Created by Akash on 29-Oct-16.
 */

public class mainUI extends Activity {

    //CardView card, card1,card2,card3,card4,card5,card6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uimain);
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        height = (height* 95)/100;

//////////////////////////FAB BUTTON CODES///////////////

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), Help2.class));

            }
        });
*/

        ///////////////////////////left side
        CardView card = (CardView) findViewById(R.id.card);
        card.getLayoutParams().height=(height/4);
        card.getLayoutParams().width=width/2;

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card));

        YoYo.with(Techniques.SlideInLeft)
                .duration(1100)
                .playOn(findViewById(R.id.card2));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card5));

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card3));

        YoYo.with(Techniques.SlideInRight)
                .duration(1100)
                .playOn(findViewById(R.id.card4));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card6));

        ImageView img = (ImageView) findViewById(R.id.image6);
//        Blurry.with(getApplicationContext()).capture(findViewById(R.id.image6)).into(img);
        // tiash


        CardView card2 = (CardView) findViewById(R.id.card2);
        card2.getLayoutParams().height=(height/4);
        card2.getLayoutParams().width=width/2;

        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), patientUI.class));

            }
        });

        CardView card5 = (CardView) findViewById(R.id.card5);
        card5.getLayoutParams().height=(height/4);
        card5.getLayoutParams().width=width/2;

        //////////////////////////////////////////////right side

        CardView card3 = (CardView) findViewById(R.id.card3);
        card3.getLayoutParams().height=height/4;
        card3.getLayoutParams().width=width/2;

        CardView card4 = (CardView) findViewById(R.id.card4);
        card4.getLayoutParams().height=height/4;
        card4.getLayoutParams().width=width/2;


        CardView card6 = (CardView) findViewById(R.id.card6);
        card6.getLayoutParams().height=height/4;
        card6.getLayoutParams().width=width/2;
        ////////////////////////////////////////////////


    }
    @Override
    public void onBackPressed() {
        // your code.
  /*
        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card));

        YoYo.with(Techniques.SlideInRight)
                .duration(1100)
                .playOn(findViewById(R.id.card2));

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card5));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card3));

        YoYo.with(Techniques.SlideInLeft)
                .duration(1100)
                .playOn(findViewById(R.id.card4));

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card6));
*/
       // startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();
        Intent intent = new Intent (Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }
}
